FROM centos:7

MAINTAINER Andrey Kanevsky <andrey@devops.co.il>

ENV container docker

RUN yum -y swap -- remove fakesystemd -- install systemd systemd-libs

RUN yum -y update; yum clean all; \
    (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
    rm -f /lib/systemd/system/multi-user.target.wants/*;\
    rm -f /etc/systemd/system/*.wants/*;\
    rm -f /lib/systemd/system/local-fs.target.wants/*; \
    rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
    rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
    rm -f /lib/systemd/system/basic.target.wants/*;\
    rm -f /lib/systemd/system/anaconda.target.wants/*;

RUN yum -y install python26 git python-configobj epel-release

RUN yum -y install python-pip gcc

RUN pip install python-statsd pyrabbit mock

#RUN rpm -ivh http://172.31.45.199:8081/artifactory/repo/com/sizmek/bigdata/diamond/4.0.60/diamond-4.0.60-2.noarch.rpm

RUN cd /opt && \
    git clone https://github.com/python-diamond/Diamond.git && \
    cd Diamond && \
    python setup.py build && \
    python setup.py install

RUN systemctl enable diamond

RUN cp /etc/diamond/diamond.conf.example /etc/diamond/diamond.conf

VOLUME [ "/sys/fs/cgroup" ]

CMD ["/usr/sbin/init"]
